import 'package:flutter/material.dart';
import 'package:travel/core/constants/color.dart';
import 'package:travel/core/constants/dismention.dart';
import 'package:travel/core/constants/textstyle.dart';

class ButtonWidget extends StatelessWidget {
  const ButtonWidget({Key? key, required this.title, this.onPress})
      : super(key: key);

  final String title;
  final Function()? onPress;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: onPress,
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: kDefaultPadding),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(kMediumPadding),
              gradient: Gradients.defaultGradientBackground),
          alignment: Alignment.center,
          child: Text(
            title,
            style: TextStyles.defaultStyle.bold.whiteTextColor,
          ),
        ));
  }
}
