import 'dart:async';

import 'package:flutter/material.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:travel/core/constants/dismention.dart';
import 'package:travel/core/constants/screen_key.dart';
import 'package:travel/core/constants/textstyle.dart';
import 'package:travel/representation/widgets/button_widget.dart';

class IntroScreen extends StatefulWidget {
  const IntroScreen({super.key});

  static String routeName = '/splash_screen';

  @override
  State<IntroScreen> createState() => _IntroScreenState();
}

class _IntroScreenState extends State<IntroScreen> {
  final PageController _pageController = PageController();

  final StreamController<int> _streamController =
      StreamController<int>.broadcast();

  @override
  void initState() {
    super.initState();
    _pageController.addListener(() {
      _streamController.add(_pageController.page!.toInt());
    });
  }

  Widget _buildItemIntroScreen(String image, String title, String description) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const SizedBox(
          height: kMediumPadding * 2,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: kMediumPadding),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Text(
              title,
              style: TextStyles.defaultStyle.bold,
            ),
            const SizedBox(
              height: kMediumPadding,
            ),
            Text(
              description,
              style: TextStyles.defaultStyle,
            ),
          ]),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: [
        PageView(
          controller: _pageController,
          children: [
            _buildItemIntroScreen('image', 'title 1', 'description 1'),
            _buildItemIntroScreen('image', 'title 1', 'description 2'),
            _buildItemIntroScreen('image', 'title 1', 'description 3'),
          ],
        ),
        Positioned(
            left: kMediumPadding,
            right: kMediumPadding,
            bottom: kMediumPadding * 2,
            child: Row(
              children: [
                Expanded(
                  flex: 6,
                  child: SmoothPageIndicator(
                    controller: _pageController,
                    count: 3,
                    effect: const ExpandingDotsEffect(
                        dotWidth: kMinPadding,
                        dotHeight: kMinPadding,
                        activeDotColor: Colors.orange),
                  ),
                ),
                StreamBuilder<int>(
                    initialData: 0,
                    stream: _streamController.stream,
                    builder: (context, snapshot) {
                      return Expanded(
                          flex: 4,
                          child: ButtonWidget(
                            title: snapshot.data != 2 ? 'Next' : 'Get started',
                            onPress: () {
                              if (_pageController.page != 2) {
                                _pageController.nextPage(
                                    duration: Duration(microseconds: 200),
                                    curve: Curves.easeIn);
                              } else {
                                Navigator.of(context)
                                    .pushNamed(ScreenKey.MAIN_SCREEN);
                              }
                            },
                          ));
                    }),
              ],
            ))
      ],
    ));
  }
}
