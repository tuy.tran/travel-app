// ignore_for_file: avoid_unnecessary_containers

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:travel/core/constants/color.dart';
import 'package:travel/core/constants/dismention.dart';
import 'package:travel/representation/widgets/app_bar_container.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  Widget _buildItemCategory(
      Widget icon, Color color, Function() onTap, String title) {
    return GestureDetector(
      onTap: onTap,
      child: Column(
        children: [
          Container(
            width: double.infinity,
            padding: EdgeInsets.symmetric(vertical: kMediumPadding),
            decoration: BoxDecoration(
                color: color.withOpacity(0.2),
                borderRadius: BorderRadius.circular(kItemPadding)),
            child: icon,
          ),
          SizedBox(height: kItemPadding),
          Text(title)
        ],
      ),
    );
  }

  Widget _header() {
    return Padding(
      padding: EdgeInsets.all(kDefaultPadding),
      child: Row(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: const [
              Text(
                'Hi Minh Tuy',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
              ),
              SizedBox(
                height: kMediumPadding,
              ),
              Text(
                'Where are you going next?',
                style: TextStyle(fontSize: 12),
              )
            ],
          ),
          Spacer(),
          Icon(
            FontAwesomeIcons.bell,
            size: kDefaultSizeIcon,
            color: Colors.white,
          ),
          SizedBox(
            width: kMinPadding,
          ),
          Container(
              width: 45,
              height: 45,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(kItemPadding),
                  color: Colors.white),
              child: Icon(
                FontAwesomeIcons.user,
                size: kDefaultSizeIcon,
                color: Colors.black,
              ))
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return AppBarContainerWidget(
      title: _header(),
      child: Container(
        child: Column(
          children: [
            TextField(
              decoration: InputDecoration(
                  hintText: 'Search your description',
                  prefixIcon: Padding(
                    padding: EdgeInsets.all(kTopPadding),
                    child: Icon(
                      FontAwesomeIcons.magnifyingGlass,
                      color: Colors.black,
                      size: kDefaultSizeIcon,
                    ),
                  ),
                  filled: true,
                  fillColor: Colors.white,
                  border: OutlineInputBorder(
                      borderSide: BorderSide.none,
                      borderRadius:
                          BorderRadius.all(Radius.circular(kItemPadding)))),
            ),
            SizedBox(
              height: kMediumPadding,
            ),
            Row(
              children: [
                Expanded(
                    child: _buildItemCategory(
                        Icon(
                          FontAwesomeIcons.hotel,
                          color: Colors.white,
                        ),
                        Color(0xFFF77777),
                        () {},
                        'Hotels')),
                SizedBox(
                  width: kDefaultPadding,
                ),
                Expanded(
                    child: _buildItemCategory(
                        Icon(
                          FontAwesomeIcons.hotel,
                          color: Colors.white,
                        ),
                        Color(0xFFF77777),
                        () {},
                        'Flights')),
                SizedBox(
                  width: kDefaultPadding,
                ),
                Expanded(
                    child: _buildItemCategory(
                        Icon(
                          FontAwesomeIcons.hotel,
                          color: Colors.white,
                        ),
                        Color(0xFFF77777),
                        () {},
                        'All'))
              ],
            )
          ],
        ),
      ),
    );
  }
}
