// ignore_for_file: use_build_context_synchronously

import 'package:flutter/material.dart';
import 'package:travel/core/constants/screen_key.dart';
import 'package:travel/core/helpers/local_storage.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  static String routeName = '/splash_screen';

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    redirectIntroScreen();
  }

  void redirectIntroScreen() async {
    await Future.delayed(const Duration(seconds: 2));
    final ignoreIntroStream =
        LocalStorageHelper.getValue(KeyLocalStorage.IGNORE_INTRO) as bool?;
    if (ignoreIntroStream != null && ignoreIntroStream) {
      Navigator.of(context).pushNamed(ScreenKey.MAIN_SCREEN);
    } else {
      LocalStorageHelper.setValue(KeyLocalStorage.IGNORE_INTRO, true);
      Navigator.of(context).pushNamed(ScreenKey.INTRO_SCREEN);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
      child: const Text(
        'Splash screen',
        style: TextStyle(
          fontWeight: FontWeight.bold,
        ),
      ),
    ));
  }
}
