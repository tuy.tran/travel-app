// ignore_for_file: constant_identifier_names

class ScreenKey {
  static const String SPLASH_SCREEN = '/splash_screen';
  static const String INTRO_SCREEN = '/intro_screen';
  static const String MAIN_SCREEN = '/main_screen';
}
