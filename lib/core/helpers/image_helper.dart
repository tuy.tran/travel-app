import 'package:flutter/material.dart';

class ImageHelper {
  static Widget loadFormAsset(String imagePath,
      {double? width,
      double? height,
      BorderRadius? radius,
      BoxFit? fit,
      Color? tinColor,
      Alignment? alignment}) {
    return ClipRRect(
      borderRadius: radius ?? BorderRadius.zero,
      child: Image.asset(
        imagePath,
        width: width,
        height: height,
        fit: fit,
        color: tinColor,
        alignment: alignment ?? Alignment.center,
      ),
    );
  }
}
