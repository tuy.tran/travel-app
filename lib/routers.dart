import 'package:flutter/material.dart';
import 'package:travel/core/constants/screen_key.dart';
import 'package:travel/representation/screens/intro_screen.dart';
import 'package:travel/representation/screens/main_app.dart';
import 'package:travel/representation/screens/splash_screen.dart';

final Map<String, WidgetBuilder> routes = {
  ScreenKey.SPLASH_SCREEN: (context) => const SplashScreen(),
  ScreenKey.INTRO_SCREEN: (context) => const IntroScreen(),
  ScreenKey.MAIN_SCREEN: (context) => const MainApp(),
};
